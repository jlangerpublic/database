<?php declare(strict_types=1);

namespace JLanger\Database;

use MyCLabs\Enum\Enum;

/**
 * Class DBReturnTypes
 * @extends Enum<int>
 * @method static self ARRAY()
 * @method static self OBJ()
 */
class DBReturnTypes extends Enum
{
    public const ARRAY = 0;
    public const OBJ   = 1;
    /**
     * DBReturnTypes constructor.
     *
     * @param int $value
     */
    public function __construct(int $value = 1)
    {
        parent::__construct($value);
    }
}
