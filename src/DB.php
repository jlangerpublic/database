<?php declare(strict_types=1);

namespace JLanger\Database;

use DateTimeImmutable;
use Exception;
use InvalidArgumentException;
use PDO;
use stdClass;

class DB
{
    /** @var PDO */
    private PDO $connection;

    /**
     * Constructor of the database
     *
     * @param DBConfig $config
     */
    public function __construct(DBConfig $config)
    {
        $dsn              =
            'mysql:host=' . $config->getHostname() .
            ';port=' . $config->getPort() .
            ';dbname=' . $config->getDatabase();
        $this->connection = new PDO($dsn, $config->getUser(), $config->getPassword());
        $this->connection->exec('set names utf8mb4');
    }

    /**
     * Selects only one element. If there is no element found it returns null.
     *
     * @param string[] $columns
     * @param string $table
     * @param string $where
     * @param string $order
     *
     * @return stdClass|null
     * @throws Exception
     */
    public function select(
        array $columns,
        string $table,
        string $where = '',
        string $order = ''
    ): ?stdClass {
        $result = $this->selectAll($columns, $table, $where, $order, 1);
        if (\is_array($result)) {
            return $result[0];
        }

        return null;
    }

    /**
     * Selects all elements from a table matching the where clause and limits it to the page size
     *
     * @param string[] $columns List of columns (either associative or indexed)
     * @param string $table Name of the table
     * @param string $where Where statement
     * @param string $order Order by statement
     * @param int|null $pageSize Entries per page
     * @param int|null $page Page to get
     *
     * @return stdClass[]
     * @throws Exception
     */
    public function selectAll(
        array $columns,
        string $table,
        string $where = '',
        string $order = '',
        ?int $pageSize = null,
        ?int $page = null
    ): ?array {
        $statement = 'SELECT ' . $this->generateSelectColumns($columns) . ' FROM ' . $table;

        if (!empty($where)) {
            $statement .= ' WHERE ' . $where;
        }
        if (!empty($order)) {
            $statement .= ' ORDER BY ' . $order;
        }
        if ($pageSize !== null) {
            if ($page === null) {
                $statement .= ' LIMIT ' . $pageSize;
            } elseif ($page > 0) {
                $offset     = ($page - 1) * $pageSize;
                $statement .= " LIMIT $offset, $pageSize";
            } else {
                throw new InvalidArgumentException('Seite muss größer sein als 0');
            }
        }

        return $this->query($statement, [], new DBReturnTypes(DBReturnTypes::OBJ));
    }

    /**
     * Generates the SQL String fot the columns to select
     * If the $columns array is an indexed array it will just take the column names.
     * If the $columns array is associative it will rename the columns.
     * so ['id' => 'ID'] will rename the id column to the ID column in the result
     *
     * @param string[] $columns
     *
     * @return string
     */
    private function generateSelectColumns(array $columns): string
    {
        if (count($columns) === 0) {
            return '*';
        }
        if (count(\array_unique(\array_values($columns))) !== count($columns)) {
            throw new InvalidArgumentException('Du hast zwei Spalten mit dem gleichen Namen');
        }
        $parsedColumns = [];
        foreach ($columns as $key => $value) {
            if (\is_int($key)) {
                $parsedColumns[] = "`$value`";
            } else {
                $this->checkValidSQLName($value);
                $parsedColumns[] = "`$key` AS '$value'";
            }
        }

        return \implode(',', $parsedColumns);
    }

    /**
     * Checks if the current column name is vulnerable to SQL injection
     *
     * @param string $columnName
     *
     * @throws InvalidArgumentException
     */
    private function checkValidSQLName(string $columnName): void
    {
        if (\is_int(\strpos($columnName, '`'))) {
            throw new InvalidArgumentException('Ungültiger Spalten- oder Tabellenname: ' . $columnName);
        }
    }

    /**
     * Creates a general sql query and executes it with parameters (see prepared statements)
     *
     * @param string             $sql
     * @param mixed[]            $parameters
     * @param DBReturnTypes|null $return
     *
     * @return mixed[]|null
     * @throws Exception
     */
    public function query(string $sql, array $parameters = [], ?DBReturnTypes $return = null): ?array
    {
        if ($return === null) {
            $return = new DBReturnTypes();
        }
        $statement = $this->connection->prepare($sql);
        $this->fixNotPrintableParameters($parameters);
        if (!$statement->execute($parameters)) {
            throw new Exception('SQL Error: ' . $statement->errorInfo()[2]);
        }
        $result = [];
        switch ($return->getValue()) {
            case DBReturnTypes::ARRAY:
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $i = 0;
                    foreach ($row as $item => $value) {
                        $columnMeta = $statement->getColumnMeta($i);
                        if ($columnMeta !== false) {
                            $row[$item] = $this->getType($columnMeta, $value);
                            $i++;
                        } else {
                            $row[$item] = (string)$value;
                        }
                    }
                    $result[] = $row;
                }
                break;
            case DBReturnTypes::OBJ:
                while ($row = $statement->fetch(PDO::FETCH_OBJ)) {
                    $i = 0;
                    foreach ($row as $item => $value) {
                        $columnMeta = $statement->getColumnMeta($i);
                        if ($columnMeta !== false) {
                            $row->$item = $this->getType($columnMeta, $value);
                            $i++;
                        } else {
                            $row->$item = (string)$value;
                        }
                    }
                    $result[] = $row;
                }
        }
        $result = count($result) === 0 ? null : $result;
        return $result;
    }

    /**
     * @param array<string,string> $columnMeta
     * @param mixed $value
     *
     * @return mixed
     * @throws Exception
     */
    private function getType(array $columnMeta, $value)
    {
        if ($value === null) {
            return null;
        }
        switch ($columnMeta['native_type']) {
            case 'LONG':
            case 'LONGLONG':
            case 'INT':
            case 'SHORT':
                return (int)$value;
            case 'TINY':
                if (\in_array($value, ['0', '1'], true)) {
                    return (bool)$value;
                } else {
                    return (int)$value;
                }
            case 'DATETIME':
            case 'DATE':
                return (new DateTimeImmutable($value));
            case 'DOUBLE':
                return (float)$value;
            default:
                return (string)$value;
        }
    }
    /**
     * Fixes non printable parameters. This is used because booleans can not be properly prepared.
     * If maria db is used, then this is not needed.
     *
     * @param mixed[] $parameters
     */
    private function fixNotPrintableParameters(array &$parameters): void
    {
        $parameters = \array_map(function ($parameter) {
            if (\is_bool($parameter)) {
                return $parameter ? '1' : '0';
            } else {
                return $parameter;
            }
        }, $parameters);
    }

    /**
     * Generates and executes an Update where in the $columnNames you specify the columns to update an the values
     * are then in the same order taken from the values array
     *
     * @param string[] $columnNames
     * @param mixed[] $values
     * @param string $where
     * @param string $table
     *
     * @throws Exception
     */
    public function update(array $columnNames, array $values, string $where, string $table): void
    {
        $setColumns = [];
        if (count($columnNames) !== count($values)) {
            throw new InvalidArgumentException('Spalten und Werte Anzahl ungleich bei UPDATE');
        }

        foreach ($columnNames as $index => $name) {
            $this->checkValidSQLName($name);
            $setColumns[] = "`$name` = ?";
        }

        $setString = \implode(',', $setColumns);

        $statement = "UPDATE $table SET $setString WHERE $where";
        $this->query($statement, $values);
    }

    /**
     * Generates and executes an insert statement via a prepared statement. In this function you need to specify all
     * values.
     *
     * @param string $table
     * @param mixed[] $values
     *
     * @throws Exception
     */
    public function insert(string $table, array $values): void
    {
        $this->checkValidSQLName($table);

        $valuePlaceholder = \implode(',', \array_fill(0, count($values), '?'));
        $statement        = "INSERT INTO `$table` VALUES($valuePlaceholder);";

        $this->query($statement, $values);
    }

    /**
     * Deletes a row / multiple rows from a table
     *
     * @param string $table
     * @param string $where
     *
     * @throws Exception
     */
    public function delete(string $table, string $where): void
    {
        $this->checkValidSQLName($table);

        $statement = "DELETE FROM $table WHERE $where;";

        $this->query($statement, [], new DBReturnTypes(DBReturnTypes::ARRAY));
    }
}
