<?php declare(strict_types=1);

namespace JLanger\Database;

use Exception;

class DBConfig
{
    private string $hostname;
    private string $port;
    private string $user;
    private string $database;
    private string $password;

    /**
     * DBConfig constructor.
     *
     * @param string|null $pathToFile
     * @param string      $method valid strings: 'json', 'xml'
     *
     * @throws \Exception
     */
    public function __construct(?string $pathToFile = null, $method = 'json')
    {
        if ($pathToFile === null) {
            return;
        }
        
        switch ($method) {
            case 'json':
                $this->loadFromJson($pathToFile);
                break;
            case 'xml':
                $this->loadFromXml($pathToFile);
                break;
            default:
                throw new \InvalidArgumentException('Invalid Method. Valid Methods are: json, xml');
        }
    }

    /**
     * Load Credentials from Json.
     *
     * @param string $pathToFile
     *
     * @return $this
     * @throws \Exception
     */
    public function loadFromJson(string $pathToFile): self
    {
        $json = \file_get_contents($pathToFile);
        if ($json === false) {
            throw new Exception('file could not be read.');
        }
        
        $config = \json_decode($json, true, 512, \JSON_THROW_ON_ERROR);
        
        if (!isset($config['database'])) {
            throw new \Exception('no database-section found in config.');
        }

        $dbconfig = $config['database'];
        
        if ($this->checkParamArray($dbconfig) === false) {
            throw new \InvalidArgumentException('database-credentials-array is incorrect.');
        }
        
        $this->hostname = $dbconfig['hostname'];
        $this->port     = $dbconfig['port'];
        $this->user     = $dbconfig['user'];
        $this->database = $dbconfig['database'];
        $this->password = $dbconfig['password'];
        
        return $this;
    }

    /**
     * Load credentials from xml-file.
     *
     * @param string $pathToDbConfig
     *
     * @return $this
     * @throws \Exception
     */
    public function loadFromXml(string $pathToDbConfig): self
    {
        $fileContent = \file_get_contents($pathToDbConfig);
        $config      = $fileContent !== false ? \simplexml_load_string($fileContent) : false;
        if ($config === false || !\file_exists($pathToDbConfig)) {
            throw new Exception('db-config-file not found.');
        }
        $this->hostname = (string)$config->database->hostname;
        $this->port     = (string)$config->database->port;
        $this->user     = (string)$config->database->user;
        $this->password = (string)$config->database->password;
        $this->database = (string)$config->database->database;

        return $this;
    }
    
    public function getHostname(): string
    {
        return $this->hostname;
    }
    
    public function setHostname(string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }
    
    public function getPort(): string
    {
        return $this->port;
    }
    
    public function setPort(string $port): self
    {
        $this->port = $port;

        return $this;
    }
    
    public function getUser(): string
    {
        return $this->user;
    }
    
    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }
    
    public function getDatabase(): string
    {
        return $this->database;
    }
    
    public function setDatabase(string $database): self
    {
        $this->database = $database;

        return $this;
    }
    
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Check if array contains the needed values.
     *
     * @param array<string, string> $params
     *
     * @return bool
     */
    private function checkParamArray(array $params) : bool
    {
        return isset($params['user'], $params['database'], $params['password'], $params['port'], $params['hostname']);
    }
}
